// ============================
//  Puerto / Port
// ============================
process.env.PORT = process.env.PORT || 3000;

// ============================
//  Entorno / Enviroment
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


// ============================
//  Base de datos
// ============================
let urlDB;

if (process.env.NODE_ENV === 'dev') {
   // urlDB = process.env.MONGO_URI;
    urlDB = 'mongodb+srv://consultas:consultas@cluster0-wv9ii.mongodb.net/persons'
} else {
    urlDB = 'mongodb://localhost:27017/mevn-crud';
}
urlDB = 'mongodb+srv://consultas:consultas@cluster0-wv9ii.mongodb.net/persons';
process.env.URLDB = urlDB;