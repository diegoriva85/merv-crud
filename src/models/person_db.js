const mongoose = require('mongoose');
const { Schema } = mongoose;

let personsSchema = new Schema({
    name: {
        type: String,
        required: [true, 'field name is required']
    },
    lastname: {
        type: String,
       required: [true, 'field lastname is required']
    },
    email: {
        type: String,
      //  unique: true,
        required: [true, 'email is required']
    },
    position: {
        type: String,
        required: [true, 'field position is required']
    },
    city: {
        type: String,
        required: [true, 'field city is required']
    },
    salary: {
        type: Number,
        default:0
    },
    hours: {
        type: Number,
       required: [true, 'field hours is required']
    },
    companies: {
        type: String,
        required: [true, 'field companies is required']
    },
    image: {
        type: Object,
        // required: [true, 'field image is required']
    }
});

module.exports = mongoose.model('persons', personsSchema);
