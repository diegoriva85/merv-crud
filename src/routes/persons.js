const express = require('express');
//const _ = require('underscore');
const router = express.Router();
//const fs = require('fs');


const fileUpload = require('express-fileupload');

router.use(fileUpload());
// const fs = require('fs');
// const path = require('path');


//models of the tables in mongo
const Persons = require('../models/person_db');
//const Producto = require('../models/producto');


router.get('/', async (req, res) => {
  const persons = await Persons.find({});
  res.json(persons);
});

router.get('/:id', async (req, res) => {
  const person = await Persons.findById(req.params.id);
  res.json(person);
});

router.post('/', async (req, res) => {
  req.body.image = req.files.image;
  const person = new Persons(req.body);
  await person.save();
  res.json({
    status: 'Person saved'
  });
});

router.put('/:id', async (req, res) => {
  await Persons.findByIdAndUpdate(req.params.id, req.body);
  res.json({
    status: 'Person Updated'
  });
});

router.delete('/:taskId', async (req, res) => {
  await Persons.findByIdAndRemove(req.params.taskId);
  res.json({
    status: 'Person deleted'
  });
});


router.put('/upload/:tipo/:id', function (req, res) {

  let tipo = req.params.tipo;
  let id = req.params.id;

  if (!req.files) {
    return res.status(400)
      .json({
        ok: false,
        err: {
          message: 'No se ha seleccionado ningún archivo'
        }
      });
  }

  // Valida tipo
  let tiposValidos = ['productos', 'usuarios'];
  if (tiposValidos.indexOf(tipo) < 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message: 'Los tipos permitidas son ' + tiposValidos.join(', ')
      }
    })
  }

  let file = req.files.file;
  let file_name = file.name.split('.');
  let ext = file_name[file_name.length - 1];

  // Extensiones permitidas
  let validExt = ['png', 'jpg', 'gif', 'jpeg'];

  if (validExt.indexOf(ext) < 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message: 'Las extensiones permitidas son ' + validExt.join(', '),
        ext: ext
      }
    })
  }

  // Cambiar nombre al archivo
  // 183912kuasidauso-123.jpg
  let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${ext}`;


  file.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {

    if (err)
      return res.status(500).json({
        ok: false,
        err
      });

  });

});



module.exports = router;