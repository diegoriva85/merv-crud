require('./config/config');
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const path = require('path');

const app = express();

//conect to DB Mongo
mongoose.connect(process.env.URLDB,
    { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true},
     (err, res) => {

    if (err) throw err;

    console.log('Data base ONLINE');

});


// settings
app.set('port', process.env.PORT);

// middlewares
app.use(morgan('dev'));
app.use(express.json());

// routes
app.use('/persons', require('./routes/persons'));

// static
app.use(express.static(path.join(__dirname, 'public')));;

// listenning on port
app.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});
